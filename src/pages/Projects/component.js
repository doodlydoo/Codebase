import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { ICONS } from '../../configs';
import ProjectSummary from '../../components/fragments/ProjectSummary';
import ProjectFilter from '../../components/fragments/ProjectFilter';
import styles from './styles.css';
import PageBase from '../../components/layouts/PageBase';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    const query = queryString.parse(location.search);

    this.state = {
      from: query.filterStartDate
        ? query.filterStartDate
        : '',
      to: query.filterEndDate
        ? query.filterEndDate
        : '',
    };
  }

  componentDidMount() {
    this.props.actions.fetchData('product', []);
    this.props.actions.fetchData('project', []);
  }

  componentDidUpdate(nextProps) {
    const { actions } = this.props;

    if (nextProps.location.search !== location.search) {
      actions.fetchData('project', []);
    }
  }

  _handleChangeSearch = (e) => {
    this.props.actions.setQuery({ name: 'search', value: e.target.value });
  };

  _handlePagination = (key, value) => {
    this.props.actions.setQuery({ name: key, value: value });
    this.props.actions.fetchData('project', []);
  };

  _handleChangeOption = (event) => {
    const query = queryString.parse(location.search);

    if (query.page) this.props.actions.setQuery({ name: 'page', value: undefined });
    if (event.isDate) {
      this.setState({ from: event.from.toISOString(), to: event.to.toISOString() });

      this.props.actions.setQuery(event);
    } else {
      this.props.actions.setQuery({ name: event.name, value: event.value });
    }
  };

  _renderSearch() {
    const query = queryString.parse(location.search);

    return (
      <div className={styles.input}>
        <input
          className={styles['input-search']}
          onChange={this._handleChangeSearch}
          placeholder="Search product/project/squad"
          type="text"
          value={query.search ? query.search : ''}
        />
        <img src={ICONS.SEARCH} />
      </div>
    );
  }

  render() {
    const { data, isLoading, meta } = this.props;
    const { from, to } = this.state;
    const metaResult = meta ? meta.totalResult !== 1 ? 'results' : 'result' : 'result';
    
    return (
      <PageBase>
        {isLoading.list ? (
          <h3>Loading...</h3>
        ) :
          (<section>
            <header className={styles.header}>
              <h3>Project Summary</h3>
              {this._renderSearch()}
            </header>
            <h5>
              Showing <strong> {meta ? `${meta.fromResult} - ` : '0'} {meta ? meta.toResult : ''} </strong>
              of <strong> {meta ? meta.totalResult : '0'}</strong> {metaResult}
            </h5>
            <aside className={styles['product-filter']}>
              <ProjectFilter
                dataProduct={data.product}
                from={from}
                isLoading={isLoading.product}
                onChangeOption={this._handleChangeOption}
                to={to}
              />
            </aside>
            <ProjectSummary
              dataProject={data.project}
              meta={meta}
              onChangeSort={this._handleChangeOption}
              onChangeUrl={this._handlePagination}
            />
          </section>)}
      </PageBase>
    );
  }
}

Component.defaultProps = {
  actions: {},
  classes: {},
  data: {},
  isLoading: {},
  location: {
    search: ''
  },
  meta: {},
};

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.object,
  isLoading: PropTypes.object,
  location: PropTypes.object,
  meta: PropTypes.object,
};
