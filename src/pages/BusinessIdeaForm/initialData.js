export default {
  id: 1,

  // === page 1
  name: '',
  productType: '',
  productName: '',
  productLogoUrl: '',
  productLogo: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  type: '',
  area: '',
  businessField: [],
  customer: '',
  otherCustomer: '',

  // === page 2
  background: '',
  backgroundPlain: '',
  desc: '',
  descPlain: '',
  telkomBenefit: '',
  telkomBenefitPlain: '',
  customerBenefit: '',
  customerBenefitPlain: '',
  valueCapture: '',
  valueCapturePlain: '',

  // === page 3
  goldenCircle: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  goldenCircleUrl: '',
  goldenCircleWhy: '',
  goldenCircleHow: '',
  goldenCircleWhat: '',
  valuePropCustomer: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  valuePropCustomerUrl: '',
  valuePropCustomerJobs: '',
  valuePropCustomerPain: '',
  valuePropCustomerGain: '',
  valuePropCustomerProductService: '',
  valuePropCustomerPainRelievers: '',
  valuePropCustomerGainCreators: '',
  leanCanvas: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  businessModel: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  additionalCanvasName: '',
  leanCanvasUrl: '',
  businessModelUrl: '',
  additionalCanvasDesc: '',

  // === page 4
  omtm: '',
  omtmDesc: '',
  omtmTarget: '',
  omtmTargetFormatted: '',
  execDate: '',

  // === page 5
  squadRequest: [
    {
      squadName: '',
      talentRequest: [
        {
          jobRole: '',
          jobLevel: '',
          amountSubmitted: '',
          amountExisting: '',
          talentExisting: [],
        },
      ],
    },
  ],
  costTalent: '',
  costEnabler: '',
  rewardForTalent: '',
  development: '',
  partnership: '',
  operational: '',
  budgetTotal: '',
};
