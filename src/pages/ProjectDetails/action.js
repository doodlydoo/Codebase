import queryString from 'query-string';
import { push } from 'react-router-redux';
import fetch from '../../utils/fetch';
import { ACTIONS } from '../../constants';
import { SERVICES, AUTHORIZATION } from '../../configs';

export function setQuery(item) {
  return (dispatch) => {
    const value = queryString.parse(location.search);
    value[item.name] = item.value;
    const urlParser = `?${queryString.stringify(value)}`;
    dispatch(push(urlParser));
  };
}

function setUrl(type, id) {
  const url = {
    details: `${SERVICES.GET_PROJECT_DETAILS}/${id}`,
    talent: `${SERVICES.GET_TALENTS_BY_PROJECT}/${id}`,
    achievement: `${SERVICES.GET_ACHIEVEMENT_BY_PROJECT}/${id}`,
  };
  return url[type];
}


export function fetchData(id, type, res) {  
  const query = queryString.parse(location.search);
  query.count = 10;

  return (dispatch) => {
    const options = {
      method: 'get',
      url: setUrl(type, id),
      params: query,
      headers: {
        Authorization: AUTHORIZATION,
      }
    };

    dispatch(loadingAction(type));

    fetch(options)
      .then(response => {
        dispatch(dataFetchedAction(response.data, type));
      })
      .catch(() => {
        dispatch(dataFetchedAction(res, type));
      });
  };
}

function dataFetchedAction(data, name) {
  return { type: ACTIONS.LIST_PROJECT_DETAILS_FETCHED, data, name };
}

function loadingAction(name) {
  return { type: ACTIONS.LOADING, name };
}
