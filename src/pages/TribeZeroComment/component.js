import React from 'react';
import TribeZeroForm from '../../components/forms/TribeZeroCommentContainer';
import data from './initialData';
import PropTypes from 'prop-types';
import FormLayout from '../../components/layouts/FormLayout';
import * as actions from './action';
import { isUrl } from '../../utils/common';
import { SERVICES } from '../../configs/index';
import Fetching from '../../components/elements/Fetching';
import getJobRoleList from '../../utils/JobRoleList';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...data,
      isLoading: false,
    };
  }

  async componentDidMount() {
    let dataFetched = {};
    this.toggleLoading(true);
    await getJobRoleList().then((res) => {
      this.setForm({ jobRoleList: res });
    });
    await actions
      .handleFetchData(this.props.match.params.id)
      .then((res) => {
        this.toggleLoading(false);
        dataFetched = res;
        this.props.openSnackbar({
          isOpen: true,
          variant: 'success',
          message: 'Your data has been successfully retrieved!',
        });
      })
      .catch(() => {
        this.toggleLoading(false);
        this.props.openSnackbar({
          isOpen: true,
          variant: 'error',
          message: 'Something went wrong. Your data can not be retrieved. ',
        });
      });
    this.setForm(dataFetched);
  }

  toggleLoading(state) {
    this.setState({ isLoading: state });
  }

  handleSaveForm(data, isComplete = false) {
    return actions.handleSendForm(data, isComplete);
  }

  setForm(data) {
    this.setState({ ...data, id: this.props.match.params.id });
  }

  _handleSetProgress = (files, progressEvent, name, setFieldValue, values) => {
    const fmtName = name.replace('Url', '');
    const progress = parseInt(Math.round((progressEvent.loaded * 100) / progressEvent.total));
    let obj = { ...values[fmtName], isFailed: false };

    if (progress >= 100) {
      obj = {
        ...obj,
        isUploaded: true,
        isFailed: false,
      };
    }

    setFieldValue(fmtName, {
      ...obj,
      progress,
    });
  };

  _handleOnFilesAdded = (files, name, setFieldValue, values) => {
    const self = this;
    const fmtName = name.replace('Url', '');

    setFieldValue(name, files);

    actions
      .handleSendImage(
        {
          id: this.props.match.params.id,
          name,
          files,
        },
        function(progressEvent) {
          self._handleSetProgress(files, progressEvent, fmtName, setFieldValue, values);
        },
      )
      .then((res) => {
        if (res === 'Network Error') {
          setFieldValue(fmtName, {
            ...values[fmtName],
            isFailed: true,
          });
          this.props.openSnackbar({
            isOpen: true,
            variant: 'error',
            message: 'Something went wrong. The image can not be uploaded.',
          });
          return;
        }

        this.props.openSnackbar({
          isOpen: true,
          variant: 'success',
          message: 'The image has been successfully uploaded!',
        });
        const image =
          name === 'productLogoUrl'
            ? SERVICES.GET_BUSINESS_IDEA_LOGO(res.data.name)
            : SERVICES.GET_BUSINESS_IDEA_CANVAS(res.data.name);
        setFieldValue(name, image);
      })
      .catch((err) => {
        if (err.response.status === 400) {
          if (err.response.data.errors.image) {
            setFieldValue(name, '');
            setFieldValue(fmtName, {
              progress: 0,
              isFailed: false,
              isUploaded: false,
            });
            this.props.openSnackbar({
              isOpen: true,
              variant: 'error',
              message: err.response.data.errors.image,
            });
            return;
          }
        }
        setFieldValue(fmtName, {
          progress: 0,
          isFailed: true,
          isUploaded: false,
        });

        this.props.openSnackbar({
          isOpen: true,
          variant: 'error',
          message: 'Something went wrong. The image can not be uploaded.',
        });
      });
  };

  _handleOnFilesRemoved = (_, name, setFieldValue, values) => {
    const fmtName = name.replace('Url', '');

    if (values[fmtName].isFailed) {
      setFieldValue(fmtName, {
        ...values[fmtName],
        progress: 0,
        isUploaded: false,
        isFailed: false,
      });
      setFieldValue(name, '');
      return;
    }

    let id;
    if (!isUrl(values[name])) {
      id = values[name];
    } else {
      const split = values[name].split('/');
      id = split[split.length - 1];
    }

    actions
      .handleDeleteImage({ id, name: fmtName })
      .then(() => {
        setFieldValue(fmtName, {
          ...values[fmtName],
          progress: 0,
          isUploaded: false,
          isFailed: false,
        });
        setFieldValue(name, '');
        this.props.openSnackbar({
          isOpen: true,
          variant: 'success',
          message: 'The image has been succesfully deleted!',
        });
      })
      .catch(() => {
        this.props.openSnackbar({
          isOpen: true,
          variant: 'error',
          message: 'Something went wrong. The image can not be deleted.',
        });
      });
  };

  render() {
    const { openSnackbar } = this.props;

    return (
      <FormLayout title="Business Idea Proposal">
        {this.state.isLoading ? (
          <Fetching />
        ) : (
          <TribeZeroForm
            {...this.state}
            getPitchingLocation={actions.fetchAdressList}
            getTribe={actions.fetchTribeList}
            handleSendForm={(data, status) =>
              this.handleSaveForm(
                {
                  id: this.props.match.params.id,
                  ...data,
                },
                status,
              )
            }
            onFilesAdded={this._handleOnFilesAdded}
            onFilesRemoved={this._handleOnFilesRemoved}
            openSnackbar={openSnackbar}
          />
        )}
      </FormLayout>
    );
  }
}

Component.propTypes = {
  actions: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  openSnackbar: PropTypes.func.isRequired,
};
