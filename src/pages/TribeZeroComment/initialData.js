export default {
  id: 1,

  // === page 1
  name: '',
  productType: '',
  productName: '',
  productLogoUrl: '',
  productLogo: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  type: '',
  area: '',
  businessField: [],
  customer: '',
  otherCustomer: '',
  feedbackGeneral: '',

  // === page 2
  background: '',
  backgroundPlain: '',
  feedbackBackground: '',
  desc: '',
  descPlain: '',
  feedbackDesc: '',
  telkomBenefit: '',
  telkomBenefitPlain: '',
  feedbackTelkomBenefit: '',
  customerBenefit: '',
  customerBenefitPlain: '',
  feedbackCustomerBenefit: '',
  valueCapture: '',
  valueCapturePlain: '',
  feedbackValueCapture: '',

  // === page 3
  goldenCircle: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  goldenCircleUrl: '',
  goldenCircleWhy: '',
  goldenCircleHow: '',
  goldenCircleWhat: '',
  feedbackGoldenCircle: '',
  valuePropCustomer: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  valuePropCustomerUrl: '',
  valuePropCustomerJobs: '',
  valuePropCustomerPain: '',
  valuePropCustomerGain: '',
  valuePropCustomerProductService: '',
  valuePropCustomerPainRelievers: '',
  valuePropCustomerGainCreators: '',
  feedbackValuePropCustomer: '',
  leanCanvas: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  businessModel: {
    progress: 0,
    isUploaded: false,
    isFailed: false,
  },
  additionalCanvasName: '',
  leanCanvasUrl: '',
  businessModelUrl: '',
  additionalCanvasDesc: '',
  feedbackAdditionalCanvas: '',

  // === page 4
  omtm: '',
  omtmDesc: '',
  omtmTarget: '',
  omtmTargetFormatted: '',
  feedbackOmtm: '',
  execDate: '',
  feedbackExecDate: '',

  // === page 5
  squadRequest: [
    {
      squadName: '',
      talentRequest: [
        {
          jobRole: '',
          jobLevel: '',
          amountSubmitted: '',
          amountExisting: '',
          talentExisting: [],
        },
      ],
    },
  ],
  feedbackSquad: '',
  costTalent: '',
  costEnabler: '',
  rewardForTalent: '',
  development: '',
  partnership: '',
  operational: '',
  budgetTotal: '',
  feedbackBudget: '',

  // Page summary
  feedbackSummary: '',
  decision: '',

  // Page recommendation
  pitchingDate: '',
  pitchingTimeStart: '',
  pitchingTimeEnd: '',
  tribe: '',
  otherTribe: '',
  address: '',
};
