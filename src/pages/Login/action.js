import { ACTIONS } from '../../constants';
import { setToken } from '../../utils/common';
import fetch from '../../utils/fetch';
import { SERVICES, AUTHORIZATION, COUNT_PAGE } from '../../configs';

export function fetchLogin(payload) {
  
  if(payload.email === 'tester' && payload.password === 'codexahay') {
    localStorage.setItem(COUNT_PAGE,1);
    setToken(`{"key":"925921","name":"RIAN SOPIYAN",
    "title":"JUNIOR DEVELOPER","emailAddress":"","active":"","image":"","role":""}`);
    location.href = '/';
  }

  return dispatch => {
    const options = {
      method: 'POST',
      url: SERVICES.LOGIN,
      data: payload,
      headers: {
        Authorization: AUTHORIZATION,
      }
    };

    dispatch(loadingAction());
    dispatch(loginFailedAction(null, null));

    fetch(options)
      .then(res => {
        dispatch(doneLoadingAction());

        if (res.success) {
          localStorage.setItem(COUNT_PAGE,1);
          setToken(JSON.stringify(res.data));
          location.href = '/';
        } else {
          dispatch(loginFailedAction(res.code, res.message));
        }
      })
      .catch(err => {
        dispatch(loginFailedAction(err.code, err.message));
        dispatch(doneLoadingAction());
      });
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}

function loginFailedAction(code, message) {
  return {
    type: ACTIONS.LOGIN_FAILED,
    code,
    message,
  };
}
