import React from 'react';
import PropTypes from 'prop-types';
import TribeZeroList from '../../components/fragments/TribeZeroTable';
import styles from './styles.css';
import services from '../../configs/services';
import socketIOClient from 'socket.io-client';
import PageBase from '../../components/layouts/PageBase';
import Fetching from '../../components/elements/Fetching';
import queryString from 'query-string';
import { ROUTES } from '../../configs';
import { Grid } from '@material-ui/core';
import EditPitchingSchedule from '../../components/forms/EditPitchingSchedule';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this._handleChangeOption = this._handleChangeOption.bind(this);
    this._handleOnChangeUrl = this._handleOnChangeUrl.bind(this);
    this._handleOnRefreshPage = this._handleOnRefreshPage.bind(this);
    this._handleOnChangeSchedule = this._handleOnChangeSchedule.bind(this);
    this._handleOnDownloadItem = this._handleOnDownloadItem.bind(this);
    this._handleOnEvaluateItem = this._handleOnEvaluateItem.bind(this);
    this._handleOnRescheduleItem = this._handleOnRescheduleItem.bind(this);
    this.state = {
      pitchingScheduleData: {
        id: '',
        pitchingDate: '',
        pitchingTimeStart: '',
        pitchingTimeEnd: '',
        address: '',
      },
    };
  }

  componentDidMount() {
    this.props.actions.getListBusinessIdea();
    this.connectSocketIO();
  }

  componentDidUpdate(nextProps) {
    const { actions } = this.props;

    if (nextProps.location.search !== location.search) {
      actions.getListBusinessIdea();
    }
  }

  connectSocketIO() {
    const socket = socketIOClient(services.BASE_URL_PMS_SOCKET);
    // On Refresh
    socket.on(`refresh:${this.props.nik}`, () => {
      this.props.actions.getListBusinessIdea();
    });
  }

  _handleChangeOption = (event) => {
    const query = queryString.parse(location.search);

    if (query.page) this.props.actions.setQuery({ name: 'page', value: undefined });
    this.props.actions.setQuery({ name: event.name, value: event.value });
  };

  _handleOnChangeUrl(query, page) {
    this.props.actions.setQuery({ name: 'page', value: page });
    this.props.actions.getListBusinessIdea();
  }

  _handleOnRefreshPage() {
    this.props.actions.getListBusinessIdea();
  }

  _handleOnRescheduleItem(data) {
    return new Promise((resolve, reject) => {
      this.props.actions
        .editPitchingSchedule(data)
        .then((res) => {
          this.props.openSnackbar({
            isOpen: true,
            variant: 'success',
            message: 'The schedule has been successfully edited!',
          });
          resolve(res);
          this.props.sideDrawerActions.closeSideDrawer();
          this._handleOnRefreshPage();
        })
        .catch((err) => {
          this.props.openSnackbar({
            isOpen: true,
            variant: 'error',
            message: 'Something went wrong. The schedule can not be edited.',
          });
          reject(err);
        });
    });
  }

  async _handleOnChangeSchedule(data) {
    const pitchingScheduleData = {
      id: data.id,
      pitchingDate: data['comment.page10_pitching_schedule'].date || '',
      pitchingTimeStart: data['comment.page10_pitching_schedule'].time_start || '',
      pitchingTimeEnd: data['comment.page10_pitching_schedule'].time_end || '',
      address: data['comment.page10_place'] || '',
    };

    await this.setState({ pitchingScheduleData });
    this.props.sideDrawerActions.openSideDrawer();
  }

  _handleOnDownloadItem(id) {
    this.props.actions.downloadBusinessIdea(id).catch(() => {
      this.props.openSnackbar({
        isOpen: true,
        variant: 'error',
        message: 'Something went wrong. Your business idea proposal can not be downloaded.',
      });
    });
  }

  _handleOnEvaluateItem(id) {
    window.open(`${ROUTES.TRIBE_ZERO_COMMENT(id)}`, '_blank');
  }

  render() {
    const { isLoading, meta, data } = this.props;

    const _renderShowingResult = () => {
      let resultMax = meta.page * 10;

      if (this.props.meta.totalData < meta.page * 10) {
        resultMax = this.props.meta.totalData;
      }

      return `${(meta.page - 1) * 10 + 1} - ${resultMax}`;
    };

    return (
      <PageBase>
        {isLoading ? (
          <Fetching />
        ) : (
          <>
            <section>
              <header className={styles.header}>
                <h3>Business Idea Evaluation Form</h3>
              </header>
              <Grid container direction="row">
                <Grid container justify="flex-start">
                  <h5>
                    Showing Results <strong>{_renderShowingResult()}</strong> of{' '}
                    <strong>{meta.totalData}</strong>
                  </h5>
                </Grid>
              </Grid>
              <TribeZeroList
                {...this.props}
                data={data}
                meta={meta}
                onChangeSchedule={this._handleOnChangeSchedule}
                onChangeSort={this._handleChangeOption}
                onChangeUrl={this._handleOnChangeUrl}
                onDownloadItem={this._handleOnDownloadItem}
                onEvaluateItem={this._handleOnEvaluateItem}
              />
            </section>
            <EditPitchingSchedule
              {...this.state.pitchingScheduleData}
              onRescheduleItem={this._handleOnRescheduleItem}
            />
          </>
        )}
      </PageBase>
    );
  }
}

Component.defaultProps = {
  classes: {},
};

Component.propTypes = {
  actions: PropTypes.object.isRequired,
  classes: PropTypes.object,
  data: PropTypes.array.isRequired,
  history: PropTypes.object.isRequired,
  isLoading: PropTypes.bool.isRequired,
  location: PropTypes.object.isRequired,
  meta: PropTypes.object.isRequired,
  nik: PropTypes.number.isRequired,
  openSnackbar: PropTypes.func.isRequired,
  sideDrawerActions: PropTypes.object.isRequired,
};
