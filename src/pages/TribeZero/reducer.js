import { ACTIONS } from '../../constants';

const initialState = {
  isLoading: false,
  data: [],
  meta: {
    page: 1,
    totalPage: 0,
    totalData: 0,
    perPage: 0,
  },
  nik: 100000,
};

export default function reducer(state = initialState, action) {
  const { type, data } = action;
  switch (type) {
    case ACTIONS.LOADING:
      return {
        ...state,
        isLoading: true,
      };
    case ACTIONS.LIST_OF_TRIBE_ZERO_FETCHED:
      return {
        ...state,
        isLoading: false,
        data: data.data,
        meta: data.meta,
      };
    case ACTIONS.EDIT_PITCHING_SCHEDULE:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
}
