import React from 'react';
import queryString from 'query-string';
import PropTypes from 'prop-types';
import ProductList from '../../components/fragments/ProductList';
import ProductFilter from '../../components/fragments/ProductFilter';
import styles from './styles.css';
import PageBase from '../../components/layouts/PageBase';

export default class Component extends React.Component {
  componentWillMount() {
    this.props.actions.fetchData('product', []);
    this.props.actions.fetchData('productFilter', []);
    this.props.actions.fetchData('tribeFilter', []);
  }

  _handleChangeSearch = (e) => {
    this.props.actions.setQuery({ name: 'src', value: e.target.value });
  };

  _handlePagination = (key, value) => {
    this.props.actions.setQuery({ name: key, value: value });
    this.props.actions.fetchData('product', []);
  };

  handleChangeOption = (event) => {
    const query = queryString.parse(location.search);

    if (query.page) this.props.actions.setQuery({ name: 'page', value: undefined });
    this.props.actions.setQuery({ name: event.name, value: event.value });
    this.props.actions.fetchData('product', []);
  };

  render() {
    const { data, isLoading, meta } = this.props;
    const metaResult = meta ? meta.totalResult !== 1 ? 'results' : 'result' : 'result';    
    
    return (
      <PageBase>
        {isLoading.product ? (
          <h3>Loading...</h3>
        ) : (
          <section>
            <header className={styles.header}>
              <h3>Product Summary</h3>
            </header>
            <h5>
              Showing 
              <strong> {meta ? `${meta.fromResult} - ` : '0'}{meta ? meta.toResult : ''}</strong> of 
              <strong> {meta ? meta.totalResult : '0'}</strong> {metaResult}
            </h5>
            <aside className={styles['product-filter']}>
              <ProductFilter
                data={{ product: data.productFilter, tribe: data.tribeFilter }}
                handleChangeOption={this.handleChangeOption}
                isLoading={{ product: isLoading.productFilter, tribe: isLoading.tribeFilter }}
              />
            </aside>
            <ProductList 
              data={data.product}
              meta={meta}
              onChangeUrl={this._handlePagination}
            />
          </section>
        )}
      </PageBase>
    );
  }
}

Component.defaultProps = {
  actions: {},
  classes: {},
  data: {},
  isLoading: {},
  meta: {},
};

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.object,
  isLoading: PropTypes.object,
  meta: PropTypes.object,
};
