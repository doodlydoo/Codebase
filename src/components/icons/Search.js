import React from 'react';

export default function ArrowNext() {
  return (
    <svg height="1rem" viewBox="0 0 16 16" width="1rem" xmlns="http://www.w3.org/2000/svg">
      <path d="M10.033 10.066a4.76 4.76 0 1 1-6.732-6.732 4.76 4.76 0 0 1 6.732 6.732zm1.28.431a6 6 0 1 0-.88.874l3.829 3.828a.62.62 0 1 0 .876-.876l-3.825-3.826z" fill="#707A89" fillRule="nonzero" />
    </svg>
  );
}
