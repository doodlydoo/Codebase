import React from 'react';
import PropTypes from 'prop-types';
import FieldText from '../../elements/FieldText';
import { Grid } from '@material-ui/core';
import { Field, FieldArray, getIn } from 'formik';
import { placeholder } from '../../../constants/copywriting';
import Button from '../../elements/Button';
import { ICONS } from '../../../configs';
import TalentProposed from './TalentProposed';

export default class Component extends React.Component {
  render() {
    const {
      jobRoleOptions,
      jobLevelOptions,
      field,
      disabled,
      errors,
      touched,
      arrayHelpers,
      setFieldTouched,
      setFieldValue,
      values,
    } = this.props;

    return (
      <>
        {field.map((item, squadIdx) => (
          <div
            key={squadIdx}
            style={{
              padding: '1rem',
              borderRadius: '0.3125rem',
              boxShadow: '0 0 1px 0 rgb(72, 122, 157)',
              marginBottom: '1rem',
            }}
          >
            <Grid alignItems="center" container direction="row" style={{ marginBottom: '1rem' }}>
              <h4>
                <strong>{`Squad ${squadIdx + 1}`}</strong>
              </h4>
              {field.length > 1 && !disabled && (
                // Button Delete Squad
                <Button
                  circle
                  onClick={() => arrayHelpers.remove(squadIdx)}
                  small
                  style={{
                    margin: 0,
                    height: '1rem',
                    backgroundColor: '#00000000',
                  }}
                >
                  <img src={ICONS.MINUS_ICON} style={{ width: '1rem', height: '1rem' }} />
                </Button>
              )}
            </Grid>
            <Field
              component={FieldText}
              disabled={disabled}
              error={getIn(errors, `squadRequest[${squadIdx}].squadName`)}
              label="Name of Squad"
              name={`squadRequest[${squadIdx}].squadName`}
              onChange={(e) => {
                setFieldValue(`squadRequest[${squadIdx}].squadName`, e.target.value);
              }}
              placeholder={placeholder.shortText('name of squad')}
              required
              touched={getIn(touched, `squadRequest[${squadIdx}].squadName`)}
            />
            <FieldArray
              name={`squadRequest[${squadIdx}].talentRequest`}
              render={(arrHelpersTalent) => (
                <TalentProposed
                  arrayHelpers={arrHelpersTalent}
                  disabled={disabled}
                  errors={errors}
                  field={field[squadIdx].talentRequest}
                  jobLevelOptions={jobLevelOptions}
                  jobRoleOptions={jobRoleOptions}
                  setFieldTouched={setFieldTouched}
                  setFieldValue={setFieldValue}
                  squadIdx={squadIdx}
                  touched={touched}
                  values={values}
                />
              )}
            />
          </div>
        ))}
        {/* Add Squad */}
        {!disabled && (
          <Button
            flat
            large
            onClick={() =>
              arrayHelpers.push({
                squadName: '',
                talentRequest: [
                  {
                    jobRole: '',
                    jobLevel: '',
                    amountSubmitted: '',
                    amountExisting: '',
                    talentExisting: [],
                  },
                ],
              })
            }
            style={{
              paddingLeft: 0,
              outline: 'none',
            }}
          >
            + Add Squad
          </Button>
        )}
      </>
    );
  }
}

Component.defaultProps = {
  className: '',
  disabled: false,
};

Component.propTypes = {
  arrayHelpers: PropTypes.object.isRequired,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  errors: PropTypes.object.isRequired,
  field: PropTypes.array.isRequired,
  jobLevelOptions: PropTypes.array.isRequired,
  jobRoleOptions: PropTypes.array.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
