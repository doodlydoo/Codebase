import React from 'react';
import { jobLevel } from '../../../../utils/dropdownList';
import { FieldArray } from 'formik';
import PropTypes from 'prop-types';
import ProposedSquadField from '../../../fragments/SquadProposedField';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { values, errors, touched, setFieldValue, setFieldTouched } = this.props;

    return (
      <>
        <h3>Resource Submission - Proposed Talent</h3>
        <p className="sub-text">
          This page identify the information about talent submission of this business idea.
        </p>
        <FieldArray
          name="squadRequest"
          render={(arrayHelpers) => (
            <ProposedSquadField
              arrayHelpers={arrayHelpers}
              errors={errors}
              field={values.squadRequest}
              jobLevelOptions={jobLevel}
              jobRoleOptions={values.jobRoleList}
              setFieldTouched={setFieldTouched}
              setFieldValue={setFieldValue}
              touched={touched}
              values={values}
            />
          )}
        />
      </>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
