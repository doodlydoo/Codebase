/* eslint-disable max-lines */
import React from 'react';
import Button from '../../elements/Button';
import { withFormik } from 'formik';
import validations from './validations';
import General from './Pages/p1General';
import ExecutiveSummary from './Pages/p2ExecutiveSummary';
import GoldenCircle from './Pages/p3GoldenCircle';
import ValuePropositionCanvas from './Pages/p4ValuePropositionCanvas';
import AdditionalCanvassing from './Pages/p5AdditionalCanvassing';
import TimeAndMetric from './Pages/p6TimeAndMetric';
import ProposedTalent from './Pages/p7ProposedTalent';
import ProposedBudget from './Pages/p8ProposedBudget';
import DownloadPage from './Pages/p9Download';
import propTypes from 'prop-types';
import _ from 'lodash';
import { Grid } from '@material-ui/core';
import BusinessIdeaFormModal from '../../fragments/BusinessIdeaFormModal';
import TalentValidation from '../../../utils/TalentValidation';

class UserForm extends React.Component {
  constructor(props) {
    super(props);
    this._handleAutosave = _.debounce(this._handleAutosave, 2000);
    this._handleBeforeSubmitting = this._handleBeforeSubmitting.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
    this.state = {
      page: 1,
      lastPage: 8,
      downloadPage: 9,
      isOpen: false,
      draftLoading: false,
    };
  }

  _handleAutosave() {
    //handle autosave
  }

  _findErrorPage() {
    const { values, errors, touched } = this.props;
    let isTalentNotValid = TalentValidation(values.squadRequest, errors, touched);
    if (
      (errors.name && touched.name) ||
      (errors.productType && touched.productType) ||
      (errors.productName && touched.productName) ||
      (errors.type && touched.type) ||
      (errors.area && touched.area) ||
      (errors.businessField && touched.businessField) ||
      (errors.customer && touched.customer) ||
      (errors.otherCustomer && touched.otherCustomer)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 1 });
      return true;
    } else if (
      (errors.backgroundPlain && touched.backgroundPlain) ||
      (errors.descPlain && touched.descPlain) ||
      (errors.telkomBenefitPlain && touched.telkomBenefitPlain) ||
      (errors.customerBenefitPlain && touched.customerBenefitPlain) ||
      (errors.valueCapturePlain && touched.valueCapturePlain)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 2 });
      return true;
    } else if (
      (errors.goldenCircleUrl && touched.goldenCircleUrl) ||
      (errors.goldenCircleWhy && touched.goldenCircleWhy) ||
      (errors.goldenCircleHow && touched.goldenCircleHow) ||
      (errors.goldenCircleWhat && touched.goldenCircleWhat)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 3 });
      return true;
    } else if (
      (errors.valuePropCustomerUrl && touched.valuePropCustomerUrl) ||
      (errors.valuePropCustomerJobs && touched.valuePropCustomerJobs) ||
      (errors.valuePropCustomerPain && touched.valuePropCustomerPain) ||
      (errors.valuePropCustomerGain && touched.valuePropCustomerGain) ||
      (errors.valuePropCustomerProductService && touched.valuePropCustomerProductService) ||
      (errors.valuePropCustomerPainRelievers && touched.valuePropCustomerPainRelievers) ||
      (errors.valuePropCustomerGainCreator && touched.valuePropCustomerGainCreator)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 4 });
      return true;
    } else if (
      (errors.additionalCanvasName && touched.additionalCanvasName) ||
      (errors.leanCanvasUrl && touched.leanCanvasUrl) ||
      (errors.businessModelUrl && touched.businessModelUrl) ||
      (errors.additionalCanvasDesc && touched.additionalCanvasDesc)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 5 });
      return true;
    } else if (
      (errors.omtm && touched.omtm) ||
      (errors.omtmDesc && touched.omtmDesc) ||
      (errors.omtmTarget && touched.omtmTarget) ||
      (errors.execDate && touched.execDate)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });

      this.setState({ page: 6 });
      return true;
    } else if (isTalentNotValid) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 7 });
      return true;
    } else if (
      (errors.costTalent && touched.costTalent) ||
      (errors.costEnabler && touched.costEnabler) ||
      (errors.rewardForTalent && touched.rewardForTalent) ||
      (errors.development && touched.development) ||
      (errors.partnership && touched.partnership) ||
      (errors.operational && touched.operational)
    ) {
      window.scrollTo({ top: 0, behavior: 'smooth' });
      this.setState({ page: 8 });
      return true;
    }
    return false;
  }

  _handleSaveDraft() {
    const isError = this._findErrorPage();
    if (!isError) {
      const { handleSendForm, values } = this.props;
      this.setState({ draftLoading: true });
      handleSendForm(values)
        .then(() => {
          this.setState({ draftLoading: false });
          this.props.openSnackbar({
            isOpen: true,
            variant: 'success',
            message: 'The form has been succesfully saved!',
          });
        })
        .catch(() => {
          this.setState({ draftLoading: false });
          this.props.openSnackbar({
            isOpen: true,
            variant: 'error',
            message: `Server error occurred. This form can not be saved.`,
          });
        });
    } else {
      this.props.openSnackbar({
        isOpen: true,
        variant: 'error',
        message: `There is invalid data input. This form can not be saved.`,
      });
    }
  }

  async _handleSubmit() {
    this.setState({ draftLoading: true });
    const { values, handleSendForm } = this.props;
    handleSendForm(values, true)
      .then(() => {
        this.setState({ draftLoading: false, isOpen: false });
        this.props.openSnackbar({
          isOpen: true,
          variant: 'success',
          message: 'The form has been succesfully submitted!',
        });
        this.setState({ page: this.state.downloadPage });
      })

      .catch(() => {
        this.setState({ draftLoading: false });
        this.props.openSnackbar({
          isOpen: true,
          variant: 'error',
          message: `Something went wrong. This form can not be submitted!`,
        });
      });
  }
  _changePage(behavior) {
    window.scrollTo({ top: 0, behavior: 'smooth' });
    this.setState({
      page: behavior === 'next' ? this.state.page + 1 : this.state.page - 1,
    });
  }
  async _handleBeforeSubmitting() {
    const { handleSubmit, errors } = this.props;
    await handleSubmit();
    if (Object.keys(errors).length > 0) {
      setTimeout(() => this._findErrorPage(), 1);
      this.props.openSnackbar({
        isOpen: true,
        variant: 'error',
        message: `Your form is incompleted yet. This form can not be submitted.`,
      });
    } else {
      this._handleToggleDialog();
    }
  }
  _handleToggleDialog = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  render() {
    const {
      values,
      errors,
      touched,
      isSubmitting,
      setFieldValue,
      setFieldTouched,
      onFilesAdded,
      onFilesRemoved,
    } = this.props;

    const attrs = {
      errors,
      id: this.props.id,
      setFieldTouched,
      setFieldValue,
      touched,
      values,
      handleAutosave: this._handleAutosave(),
      onFilesAdded,
      onFilesRemoved,
    };

    return (
      <>
        <BusinessIdeaFormModal
          isOpen={this.state.isOpen}
          onHandleClose={this._handleToggleDialog}
          onHandleSubmit={this._handleSubmit}
        />
        <form method="POST">
          {this.state.page === 1 && <General {...attrs} />}
          {this.state.page === 2 && <ExecutiveSummary {...attrs} />}
          {this.state.page === 3 && <GoldenCircle {...attrs} />}
          {this.state.page === 4 && <ValuePropositionCanvas {...attrs} />}
          {this.state.page === 5 && <AdditionalCanvassing {...attrs} />}
          {this.state.page === 6 && <TimeAndMetric {...attrs} />}
          {this.state.page === 7 && <ProposedTalent {...attrs} />}
          {this.state.page === 8 && <ProposedBudget {...attrs} />}
          {this.state.page === this.state.downloadPage && <DownloadPage {...attrs} />}
          {this.state.page !== this.state.downloadPage && (
            <Grid container direction="row" justify="flex-end" style={{ marginTop: '2.5rem' }}>
              {this.state.page !== 1 && (
                <Button
                  data-cy="prev"
                  ghost
                  large
                  onClick={() => this._changePage('previous')}
                  type="button"
                >
                  Previous
                </Button>
              )}
              <Button
                data-cy="save-draft"
                disabled={this.state.draftLoading}
                isloading={this.state.draftLoading}
                large
                onClick={() => this._handleSaveDraft()}
                type="button"
              >
                Save Draft
              </Button>
              {this.state.page !== this.state.lastPage ? (
                <Button data-cy="next" large onClick={() => this._changePage('next')} type="button">
                  Next
                </Button>
              ) : (
                <Button
                  data-cy="submit"
                  disabled={isSubmitting}
                  isloading={isSubmitting}
                  large
                  onClick={this._handleBeforeSubmitting}
                  type="button"
                >
                  Submit
                </Button>
              )}
            </Grid>
          )}
        </form>
      </>
    );
  }
}

const MyEnhancedForm = withFormik({
  enableReinitialize: true,
  validationSchema: validations,
  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      setSubmitting(false);
    }, 400);
  },
  displayName: 'BusinessIdeaForm',
})(UserForm);

UserForm.propTypes = {
  errors: propTypes.object.isRequired,
  handleSendForm: propTypes.func.isRequired,
  handleSubmit: propTypes.func.isRequired,
  id: propTypes.number.isRequired,
  isSubmitting: propTypes.bool.isRequired,
  isValid: propTypes.bool.isRequired,
  onFilesAdded: propTypes.func.isRequired,
  onFilesRemoved: propTypes.func.isRequired,
  openSnackbar: propTypes.func.isRequired,
  setFieldTouched: propTypes.func.isRequired,
  setFieldValue: propTypes.func.isRequired,
  touched: propTypes.object.isRequired,
  values: propTypes.object.isRequired,
};

export default MyEnhancedForm;
