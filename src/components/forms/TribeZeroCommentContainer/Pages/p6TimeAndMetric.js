import React from 'react';
import Grid from '@material-ui/core/Grid';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import FieldDate from '../../../elements/FieldDate';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { errors, touched, setFieldTouched, setFieldValue, handleAutosave, values } = this.props;

    return (
      <React.Fragment>
        <h3>Time of Implementation and Metric</h3>
        <p className="sub-text">
          This page identify the plan of team&#39;s target along its explanation.
        </p>
        <FieldDate
          data-cy="exec-date"
          dateFormat="DD/MM/YYYY"
          disabled
          isRange
          label="Time of Implementation"
          placeholder="Start Date ~ End Date"
          required
          useFormattedValue
          value={values.execDate}
        />

        <Field
          name="feedbackExecDate"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-exec-date"
              error={errors.feedbackExecDate}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Time of Implementation"
              onChange={(e) => {
                setFieldTouched('feedbackExecDate', true);
                setFieldValue('feedbackExecDate', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`evaluation of time of implementation`, 700)}
              required
              touched={touched.feedbackExecDate}
              type="long"
            />
          )}
        />

        <Grid container direction="column">
          <Grid className="field-group" container>
            <Grid item>
              <FieldText
                data-cy="omtm-name"
                disabled
                label="Metric OMTM"
                long
                placeholder={placeholder.shortText('metric OMTM')}
                required
                type="text"
                value={values.omtm}
              />
            </Grid>
            <Grid item>
              <FieldText
                data-cy="omtm-target"
                decimal
                disabled
                label="Target"
                long
                max={12}
                placeholder={placeholder.shortText('the target')}
                required
                type="number"
                value={values.omtmTargetFormatted}
              />
            </Grid>
          </Grid>

          <FieldText
            block
            data-cy="omtm-desc"
            disabled
            label="Explanation"
            placeholder={placeholder.longText('the explanation', 700)}
            required
            type="long"
            value={values.omtmDesc}
          />

          <Field
            name="feedbackOmtm"
            render={({ field }) => (
              <FieldText
                {...field}
                block
                data-cy="feedback-omtm"
                error={errors.feedbackOmtm}
                info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
                label="Evaluation of Metric OMTM"
                onChange={(e) => {
                  setFieldTouched('feedbackOmtm', true);
                  setFieldValue('feedbackOmtm', e.target.value);
                  handleAutosave;
                }}
                placeholder={placeholder.longText(`evaluation of metric OMTM`, 700)}
                required
                touched={touched.feedbackOmtm}
                type="long"
              />
            )}
          />
        </Grid>
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
