import React from 'react';
import renderer from 'react-test-renderer';
import Component from '../component';

jest.mock('redux-form');
let props = {};

describe('Component', () => {
  it('renders correctly', () => {
    props.initialize = jest.fn();
    const tree = renderer
      .create(<Component classes={{}} {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders correctly with message', () => {
    props.initialize = jest.fn();
    const message = 'test';
    const tree = renderer
      .create(<Component classes={{}} message={message} {...props} />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
