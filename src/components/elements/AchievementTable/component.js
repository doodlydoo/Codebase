import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '../Pagination';
import styles from './styles.css';

export default class Component extends React.Component {
  render() {
    const { data, isLoading, meta, onChangeUrl } = this.props;

    if(isLoading || !data.length  )
      return null;

    return (
      <>
        <table className={styles.table}>
          <thead>
            <tr>
              <th>Objective</th>
              <th>Key Result</th>
            </tr>
          </thead>
          <tbody className={styles.tbody}>
            {data.map((item, idx) => (
              <tr key={idx}>
                <td>{item.objective}</td>
                <td>{item.keyResult}</td>
              </tr>  
            ))}
          </tbody>
        </table>
        <Pagination className={styles.pagination} meta={meta} onChangeUrl={onChangeUrl} />
      </>
    );
  }
}

Component.defaultProps = {
  data: [],
  isLoading: true,
  match: {},
  meta: {},
  onChangeUrl: {},
};

Component.propTypes = {
  data: PropTypes.array,
  isLoading: PropTypes.bool,
  match: PropTypes.object,
  meta: PropTypes.object,
  onChangeUrl: PropTypes.func,
};
