import React from 'react';
import Snackbar from './Snackbar';
import PropTypes from 'prop-types';

export default class SnackbarContainer extends React.Component {
  render() {
    return <Snackbar {...this.props.snackbar} />;
  }
}

SnackbarContainer.propTypes = {
  snackbar: PropTypes.object.isRequired
};
