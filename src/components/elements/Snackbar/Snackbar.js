import React from 'react';
import PropTypes from 'prop-types';
import { SnackbarContentWrapper } from './SnackbarContentWrapper';
import Snackbar from '@material-ui/core/Snackbar';
import { withStyles } from '@material-ui/core/styles';

const styles2 = theme => ({
  margin: {
    margin: theme.spacing.unit
  }
});

class CustomizedSnackbars extends React.Component {
  state = {
    open: this.props.isOpen
  };

  componentDidUpdate(_, prevState) {
    const { isOpen } = this.props;

    if (prevState.open !== isOpen) {
      this.setState({ open: isOpen });
    }
  }

  handleClick = () => {
    this.setState({ open: true });
  };

  _handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  };

  render() {
    const { variant, message } = this.props;

    return (
      <div>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          autoHideDuration={5000}
          onClose={this._handleClose}
          open={this.state.open}
        >
          <SnackbarContentWrapper message={message} onClose={this._handleClose} variant={variant} />
        </Snackbar>
      </div>
    );
  }
}

CustomizedSnackbars.defaultProps = {
  isOpen: false
};

CustomizedSnackbars.propTypes = {
  classes: PropTypes.object.isRequired,
  isOpen: PropTypes.bool,
  message: PropTypes.string.isRequired,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired
};

export default withStyles(styles2)(CustomizedSnackbars);
