import React from 'react';
import renderer from 'react-test-renderer';
import Component from '../component';
import { Formik, Field } from 'formik';

describe('Field Input', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(
        <Formik>
          <Field component={Component} name="fname" required />
        </Formik>
      )
      .toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('renders correctly', () => {
    const tree = renderer.create(<Component long required={false} />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders with error', () => {
    const tree = renderer
      .create(<Component error="this is error message" required small touched />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders Text Area', () => {
    const tree = renderer.create(<Component block label="Text Area Input" type="long" />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders Text Area with error', () => {
    const tree = renderer
      .create(
        <Component
          block
          error="this is error message"
          label="Text Area Input"
          touched
          type="long"
        />
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders Input Type Rupiah', () => {
    const tree = renderer.create(<Component label="Text Area Input" type="rupiah" />).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders Input Type Number with decimal', () => {
    const tree = renderer
      .create(<Component decimal label="Text Area Input" type="number" />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders Input Type password', () => {
    const tree = renderer.create(<Component type="password" />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
