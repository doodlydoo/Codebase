import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';
import CircularProgress from '@material-ui/core/CircularProgress';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this._getClass = this._getClass.bind(this);
  }

  _getClass() {
    let className = this.props.circle ? styles['cdx-btn-circle'] : styles['cdx-btn'];
    if (this.props.ghost) {
      className = `${className} ${styles['ghost']}`;
    }
    if (this.props.isloading) {
      className = `${className} ${styles['loading']}`;
    }
    if (this.props.flat) {
      className = `${className} ${styles['flat']}`;
    }
    if (this.props.delete) {
      className = `${className} ${styles['delete']}`;
    }
    if (this.props.small) {
      className = `${className} ${styles['small']}`;
    }
    if (this.props.large) {
      className = `${className} ${styles['large']}`;
    }
    if (this.props.block) {
      className = `${className} ${styles['block']}`;
    }

    return className;
  }

  _getProps = () => {
    let array = this.props;
    let filtered = {};

    const allowed = ['block', 'circle', 'delete', 'flat', 'ghost', 'isloading', 'large', 'small'];
    Object.keys(array).forEach(key => {
      if (!allowed.includes(key)) {
        filtered = {
          ...filtered,
          [key]: array[key]
        };
      }
    });

    return filtered;
  };

  render() {
    const { type, icon, children, className, isloading } = this.props;

    return (
      <button {...this._getProps()} className={`${this._getClass()} ${className}`} type={type}>
        {isloading ? (
          <CircularProgress color="inherit" size={24} style={{ verticalAlign: 'middle' }} />
        ) : (
          <>
            {icon && <img src={icon} style={{ verticalAlign: 'middle', marginRight: 8 }} />}
            {children}
          </>
        )}
      </button>
    );
  }
}

Component.defaultProps = {
  block: false,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string
  ]),
  circle: false,
  className: '',
  delete: false,
  flat: false,
  ghost: false,
  icon: '',
  isloading: false,
  large: false,
  small: false,
  type: 'Button'
};

Component.propTypes = {
  block: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
    PropTypes.number,
    PropTypes.string
  ]),
  circle: PropTypes.bool,
  className: PropTypes.string,
  delete: PropTypes.bool,
  flat: PropTypes.bool,
  ghost: PropTypes.bool,
  icon: PropTypes.string,
  isloading: PropTypes.bool,
  large: PropTypes.bool,
  small: PropTypes.bool,
  type: PropTypes.string
};
