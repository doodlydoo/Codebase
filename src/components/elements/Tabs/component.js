import React from 'react';
import PropTypes from 'prop-types';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import styles from './styles.css';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0
    };
  }

  _handleChange = (event, newValue) => {
    this.setState({ value: newValue });
  };

  renderContentTab(item, idx) {
    return (
      <div
        hidden={this.state.value !== idx}
        index={1}
        key={idx}
        role="tabpanel"
        value={this.state.value}
      >
        {item}
      </div>
    );
  }

  render() {
    const { content, title } = this.props;

    return (
      <>
        <header className={styles.main}>
          <Tabs
            aria-label="disabled tabs example"
            classes={{
              root: styles['tab-header'],
              indicator: styles['tab-indicator']
            }}
            indicatorColor="primary"
            onChange={this._handleChange}
            textColor="primary"
            value={this.state.value}
          >
            {title.map((item, idx) => (
              <Tab
                classes={{
                  root: styles['tab-content'],
                  selected: styles['tab-selected']
                }}
                key={idx}
                label={<span className={styles['tab-label']}>{item}</span>}
              />
            ))}
          </Tabs>
        </header>
        {content.map((item, idx) => this.renderContentTab(item, idx))}
      </>
    );
  }
}

Component.defaultProps = {
  content: ['', ''],
  title: ['Talents', 'OKR'],
};

Component.propTypes = {
  content: PropTypes.array,
  title: PropTypes.array,
};
