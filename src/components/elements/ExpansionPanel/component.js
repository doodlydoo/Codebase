import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.css';

export default class Component extends React.Component {
  _renderOkr() {
    return (
      <table className={styles.table}>
        <thead>
          <tr>
            <th>Objective</th>
            <th>Key Result</th>
          </tr>
        </thead>
        <tbody className={styles.tbody}>
          <tr>
            <td>
              <div className={styles['objective']} />
            </td>
            <td>
              <div className={styles['progressbar-container']} />
            </td>
          </tr>
          <tr>
            <td>
              <div className={styles['objective']} />
            </td>
            <td>item.keyResult</td>
          </tr>
        </tbody>
      </table>
    );
  }

  render() {
    return (
      <div className={styles.container}>
        <header className={styles.header}>
          <h5>Objective : Launch a blockbuster goals management product by end of FY 2017-18</h5>
        </header>
        <section>
          {this._renderOkr()}
        </section>
      </div>
    );
  }
}

Component.defaultProps = {
  data: [],
};

Component.propTypes = {
  data: PropTypes.array,
};
