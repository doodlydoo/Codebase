import axios from 'axios';
import { SERVICES } from '../configs';

export default function getJobRoleList() {
  return new Promise((resolve) => {
    axios
      .get(SERVICES.GET_LIST_JOB_ROLE, { headers: { 'x-api-key': 'ynFV0UIdfrBMPipO' } })
      .then((res) => {
        let jobRoleList = [];
        const data = res.data.data.data;
        data.map((job) => {
          jobRoleList.push({ label: job.title, value: job.title });
        });
        resolve(jobRoleList);
      });
  });
}
