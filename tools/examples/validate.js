import { errorHandler } from '../../../constants/copywriting';
import * as Yup from 'yup';

export default Yup.object().shape({
  field1: Yup.string().required(errorHandler.required('Field One')),
  field2: Yup.string().required(errorHandler.required('Field Two')),
});
